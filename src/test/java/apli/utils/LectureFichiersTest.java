package apli.utils;

import apli.modele.DistanceVilles;
import apli.modele.Membre;
import apli.modele.Vente;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LectureFichiersTest {

    @Test
    void lireFichierMembres() {
        try {
            File fichier = LectureFichiers.getFile("./membres_APLI.txt");
            List<Membre> listMembres = LectureFichiers.lireFichierMembres(fichier);
            assertEquals(384, listMembres.size(), "la taille de la liste des membres est incorrecte");

            System.out.println(listMembres);
        } catch(IOException e) {
            e.printStackTrace();
            assertFalse(false, "IOException");
        }
    }
    @Test
    void lireFichierScenerio() {
        try {
            File fichier = LectureFichiers.getFile("./scenario_0.txt");
            List<Vente> listVentes = LectureFichiers.lireFichierScenario(fichier);
            assertEquals(3, listVentes.size(), "la taille de la liste des ventes est incorrecte");

            Vente vente1 =listVentes.get(0);
            assertEquals("Sabelettenote", vente1.getPseudoAcheteur());
            assertEquals("Kokiyas", vente1.getPseudoVendeur());
            System.out.println(listVentes);
        } catch(IOException e) {
            e.printStackTrace();
            assertFalse(false, "IOException");
        }
    }

    @Test
    void lireFichierDistances() {
        try {
            File fichier = LectureFichiers.getFile("./distances.txt");
            LectureFichiers.initialiserDistanceVilles(fichier);
            assertEquals(369, DistanceVilles.getInstance().calculDistance("Amiens", "Angers"), "distance incorrecte");
            assertEquals(369, DistanceVilles.getInstance().calculDistance("Angers", "Amiens"), "distance incorrecte");
            assertEquals(0, DistanceVilles.getInstance().calculDistance("Angers", "Angers"), "distance incorrecte");

        } catch(IOException e) {
            e.printStackTrace();
            assertFalse(false, "IOException");
        }
    }
}