package apli.utils;

import apli.modele.*;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AlgorithmeSolutionIdealeTest {

    @Test
    void testScenario_0() {
        Trajet meilleurTrajet = trouverSolutionIdeale("scenario_0.txt");
        assertEquals(meilleurTrajet.getDistanceTrajet(), 2266 );
    }

    @Test
    void testScenario_1_1() {
        Trajet meilleurTrajet = trouverSolutionIdeale("scenario_1_1.txt");
        assertEquals(meilleurTrajet.getDistanceTrajet(), 4558 );
    }

    @Test
    void testScenario_2_1() {
        Trajet meilleurTrajet = trouverSolutionIdeale("scenario_2_1.txt");
        assertEquals(meilleurTrajet.getDistanceTrajet(), 14287 );
    }

    private Trajet trouverSolutionIdeale(String scenario) {
        try {
            // On récupère les distances entre les villes
            File fichierVilles = LectureFichiers.getFile("distances.txt");
            LectureFichiers.initialiserDistanceVilles(fichierVilles);

            // On récupère les membres et on les met dans ListMembres
            File fichierMembres = LectureFichiers.getFile("membres_APLI.txt");
            List<Membre> listMembres = LectureFichiers.lireFichierMembres(fichierMembres);
            ListMembres.getInstance().addAll(listMembres);

            List<Vente> ventes = LectureFichiers.lireFichierScenario(LectureFichiers.getFile(scenario));
            Algorithme algo = new AlgorithmeSolutionIdeale();
            List<Trajet> listTrajet = algo.calculer(VenteDetail.toListVenteDetail(ventes));

            Iterator<Trajet> iterator = listTrajet.iterator();
            while (iterator.hasNext()) {
                Trajet trajet = iterator.next();
                System.out.println(trajet.toString());
            }

            return listTrajet.get(0);

        } catch(IOException e) {
            e.printStackTrace();
            assertFalse(false, "IOException");
        }
        return null;
    }
}