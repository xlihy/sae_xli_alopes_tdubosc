package apli.utils;

import apli.modele.DistanceVilles;
import apli.modele.Trajet;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class TrajetTest {

    @Test
    void testDistanceTrajet() {
        try {
            File fichier = LectureFichiers.getFile("./distances.txt");
            LectureFichiers.initialiserDistanceVilles(fichier);
            DistanceVilles distanceVilles = DistanceVilles.getInstance();

            Trajet trajet = new Trajet();
            trajet.ajouterVille("Amiens");
            trajet.ajouterVille("Angers");
            trajet.ajouterVille("Biarritz");

            assertEquals(887, trajet.getDistanceTrajet());

        } catch(IOException e) {
            e.printStackTrace();
            assertFalse(false, "IOException");
        }
    }
}