package apli.utils;

import apli.modele.DistanceVilles;
import apli.modele.Membre;
import apli.modele.Vente;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class LectureFichiers {

    public static File getFile(String fileName) throws IOException {
        URL resource = ClassLoader.getSystemResource(fileName);

        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }

    public static List<Membre> lireFichierMembres(File fichier) throws IOException {
        //Permet de lire un fichier membre et renvoie une listes des membres
        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        List<Membre> result = new ArrayList<Membre>();
        String ligne;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                // Extraire pseudo et ville de ligne
                StringTokenizer st = new StringTokenizer(ligne);
                String pseudo = st.nextToken();
                String ville = st.nextToken();
                Membre membre = new Membre(pseudo, ville);
                result.add(membre);
            }
        } while(ligne != null);
        bufferEntree.close();
        return result;
    }

    public static List<Vente> lireFichierScenario(File fichier) throws IOException {
        //Permet de lire un fichier Scenario et renvoie une liste des differrents trajets d'un scenario
        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        List<Vente> result = new ArrayList<Vente>();
        String ligne;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                // Extraire acheteur et vendeurs de ligne
                StringTokenizer st = new StringTokenizer(ligne);
                String vendeur = st.nextToken();
                st.nextToken();
                String acheteur = st.nextToken();
                Vente vente = new Vente (vendeur, acheteur);
                result.add(vente);
            }
        } while(ligne != null);
        bufferEntree.close();
        return result;
    }

    public static void initialiserDistanceVilles(File fichier) throws IOException {
        BufferedReader bufferEntree = new BufferedReader(new FileReader(fichier));
        DistanceVilles distanceVilles = DistanceVilles.getInstance();
        String ligne;
        int indexVille = 0;
        do {
            ligne = bufferEntree.readLine();
            if (ligne != null) {
                // Extraire acheteur et vendeurs de ligne
                StringTokenizer st = new StringTokenizer(ligne);
                String ville = st.nextToken();
                List <Integer> kms = new ArrayList<Integer>();
                while (st.hasMoreTokens()) {
                    kms.add(Integer.valueOf(st.nextToken()));
                }
                distanceVilles.ajouterVille(indexVille, ville, kms);
                indexVille++;
            }
        } while(ligne != null);
        bufferEntree.close();
    }
}
