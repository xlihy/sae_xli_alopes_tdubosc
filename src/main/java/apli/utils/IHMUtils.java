package apli.utils;

import javafx.scene.control.Alert;

public class IHMUtils {
    public static void afficherMessageInfo(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText("Attention");
        alert.setContentText(message);

        alert.showAndWait();
    }
}
