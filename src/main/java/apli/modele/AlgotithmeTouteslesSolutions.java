package apli.modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AlgotithmeTouteslesSolutions  extends Algorithme {
    @Override
    public List<Trajet> calculer(List<VenteDetail> scenario) {
        // Creation du graphe
        GrapheDAG graphe = new GrapheDAG(scenario);
        // Calcul des ordres topologiques
        List<List<String>> listeOrdresTopologiques = calculerSolutions(graphe);

        // transformation en Liste de trajets
        List<Trajet> listTrajets = new ArrayList<>();
        Iterator<List<String>> iterator = listeOrdresTopologiques.iterator();
        int i = 1;
        while(iterator.hasNext()) {
            List<String> ordreTopologique = iterator.next();
            Trajet trajet = retournerTrajetAPartirOrdreTopologique(ordreTopologique);
            trajet.setNom("Trajet " + i++);
            listTrajets.add(trajet);
        }

        return listTrajets;
    }


    private List<List<String>> calculerSolutions(GrapheDAG graphe) {
        // Méthode récurrente qui permet de calculer tous les ordres topologiques d'un graphe
        List<String> sources = graphe.trouverToutesLesSources();
        List<List<String>> solutions = new ArrayList<List<String>>();
        // Si pas de source, on renvoie un ordre topologique vide
        if (sources.size() == 0){
            solutions.add(new ArrayList<>());
        }
        //Pour chaque source
        Iterator<String> iterator = sources.iterator();
        while(iterator.hasNext()){
            String source = iterator.next();
            // Plus petit graphe sans la source
            GrapheDAG grapheMoinsSource = graphe.creerPlusPetitGrapheSansSource(source);
            // Calcul récursif des solutions du plus petit graphe
            List<List<String>> listOrdresTopologiques = calculerSolutions(grapheMoinsSource);
            Iterator<List<String>> listIterator = listOrdresTopologiques.iterator();
            // POur chque ordre topologiue du petit graphe
            while (listIterator.hasNext()) {
                List<String> ordreTopologiqueGrapheMoinsSource = listIterator.next();
                // Ajouter la source au début
                ordreTopologiqueGrapheMoinsSource.add(0, source);
                // Ajouter aux solutions du graphe courant
                solutions.add(ordreTopologiqueGrapheMoinsSource);
            }
        }
        return solutions;
    }
}