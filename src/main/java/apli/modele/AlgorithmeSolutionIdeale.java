package apli.modele;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AlgorithmeSolutionIdeale  extends AlgoritmeTousLesSolutionsOrdreCroissant {
    @Override
    public List<Trajet> calculer(List<VenteDetail> scenario) {
        List<Trajet> trajets = super.calculer(scenario);

        List<Trajet> solutions = new ArrayList<>();
        Trajet trajet = trajets.get(0);
        trajet.setNom("Trajet 1");
        solutions.add(trajet);

        return  solutions;
    }
}
