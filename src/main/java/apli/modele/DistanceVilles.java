package apli.modele;

import java.util.HashMap;
import java.util.List;

public class DistanceVilles {

    private static DistanceVilles distanceVilles;

    static public DistanceVilles getInstance() {
        if (DistanceVilles.distanceVilles == null) {
            DistanceVilles.distanceVilles = new DistanceVilles();
        }
        return DistanceVilles.distanceVilles;
    }
    /*
    Clé = nom ville
    Valeur = liste des distances associées
     */
    private HashMap<String, List<Integer>> tableauxDistances;
    /*
    Clé = nom ville
    Valeur = indice dans le liste des distances
     */
    private HashMap<String, Integer> mapVilletoIndex;

    public DistanceVilles() {
        //Constructeur principal
        tableauxDistances = new HashMap<>();
        mapVilletoIndex = new HashMap<>();
    }

    public void ajouterVille(int index, String ville, List<Integer> distances) {
        //
        tableauxDistances.put(ville,distances);
        mapVilletoIndex.put(ville, index);
    }

    public int calculDistance(String ville1, String ville2) {
        //
        List<Integer> distances = tableauxDistances.get(ville1);
        Integer indexVille2 = mapVilletoIndex.get(ville2);
        return distances.get(indexVille2);
    }

}
