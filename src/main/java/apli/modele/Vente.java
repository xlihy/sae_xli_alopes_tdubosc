package apli.modele;

public class Vente {
    private String pseudoAcheteur;
    private String pseudoVendeur;

    public Vente(String pseudoVendeur, String pseudoAcheteur) {
        //Constructeur principal
        this.pseudoVendeur = pseudoVendeur;
        this.pseudoAcheteur = pseudoAcheteur;
    }

    @Override
    public String toString() {
        return "Vente{" +
                "pseudoVendeur='" + pseudoVendeur + '\'' +
                ", pseudoAcheteur='" + pseudoAcheteur + '\'' +
                '}';
    }

    public VenteDetail toVenteDetail() {
        // Transforme un objet Vente en VenteDetail, notamment pour l'affichage de la vue Scenario
        VenteDetail venteDetail = new VenteDetail(pseudoVendeur, pseudoAcheteur);
        venteDetail.setVilleAcheteur(ListMembres.trouverVilleDunMembre(pseudoAcheteur));
        venteDetail.setVilleVendeur(ListMembres.trouverVilleDunMembre(pseudoVendeur));
        venteDetail.setDistance("" + DistanceVilles.getInstance().calculDistance(venteDetail.getVilleAcheteur(), venteDetail.getVilleVendeur()));
        return venteDetail;
    }

    public String getPseudoAcheteur() {
        return pseudoAcheteur;
    }

    public String getPseudoVendeur() {
        return pseudoVendeur;
    }
}
