package apli.modele;

import apli.modele.Vente;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VenteDetail extends Vente {
    private String villeAcheteur;
    private String villeVendeur;
    private String distance;

    public VenteDetail(String pseudoVendeur, String pseudoAcheteur) {
        super(pseudoVendeur, pseudoAcheteur );
    }

    public void setVilleAcheteur(String villeAcheteur) {
        this.villeAcheteur = villeAcheteur;
    }

    public void setVilleVendeur(String villeVendeur) {
        this.villeVendeur = villeVendeur;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getVilleAcheteur() {
        return villeAcheteur;
    }

    public String getVilleVendeur() {
        return villeVendeur;
    }

    public String getDistance() {
        return distance;
    }

    public static List<VenteDetail> toListVenteDetail(List<Vente> ventes) {
        List<VenteDetail> venteDetailList = new ArrayList<VenteDetail>();
        Iterator<Vente> iterator = ventes.iterator();
        while (iterator.hasNext()) {
            Vente vente = iterator.next();
            VenteDetail venteDetail = vente.toVenteDetail();
            venteDetailList.add(venteDetail);
        }
        return venteDetailList;
    }
}
