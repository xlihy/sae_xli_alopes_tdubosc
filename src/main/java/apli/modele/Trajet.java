package apli.modele;

import java.util.ArrayList;
import java.util.List;

public class Trajet implements  Comparable<Trajet> {
    private List<String> villes;
    private String nom;

    public Trajet() {
        //Constructeur principal
        villes = new ArrayList<String>();
    }

    public void ajouterVille(String ville) {

        villes.add(ville);
    }

    public int getDistanceTrajet() {
        //Permet de calculer la distance total entre les différentes villes d'un trajet d'en l'orde de la liste
        int distanceTotale = 0;
        for (int i = 0; i < villes.size() - 1; i++) {
            String ville1 = villes.get(i);
            String ville2 = villes.get(i+1);
            distanceTotale = distanceTotale + DistanceVilles.getInstance().calculDistance(ville1, ville2);

        }

        return distanceTotale;
    }

    @Override
    public String toString() {
        return "Trajet{" +
                "nom=" + nom +
                ",villes=" + villes +
                '}';
    }

    public List<Etape> getListEtapes() {
        List<Etape> listEtapes = new ArrayList<>();
        for (int i = 0; i < villes.size() - 1; i++) {
            String ville1 = villes.get(i);
            String ville2 = villes.get(i + 1);
            Etape etape = new Etape(ville1, ville2);
            listEtapes.add(etape);
        }
        return listEtapes;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public List<String> getVilles() {
        return villes;
    }


    @Override
    public int compareTo(Trajet autreTrajet) {
        int distanceTotale = getDistanceTrajet();
        int distanceTotaleAutreTrajet = autreTrajet.getDistanceTrajet();

        if (distanceTotale == distanceTotaleAutreTrajet) return  0;
        if (distanceTotale > distanceTotaleAutreTrajet) return  1;
        return -1;
    }
}
