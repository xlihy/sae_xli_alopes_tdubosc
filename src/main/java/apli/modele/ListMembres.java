package apli.modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListMembres {
    private static List<Membre> listMembres = new ArrayList<>();

    public static String trouverVilleDunMembre(String pseudo) {
        //Permet de récupérer la ville d'un membre grâce a son pseudo et la liste des membres
        Iterator<Membre> it = listMembres.iterator();
        while(it.hasNext()) {
            Membre membre = it.next();
            if (membre.getPseudo().equals(pseudo)){
                return membre.getVille();
            }
        }
        return null;
    }
    public static List<Membre> getInstance() {
        return listMembres;
    }

}
