package apli.modele;

import apli.modele.DistanceVilles;

import java.util.Objects;

public class Etape {
    private String villeDepart;
    private String villeArrivee;
    private String distance;

    public Etape(String villeDepart, String villeArrivee) {
        this.villeArrivee = villeArrivee;
        this.villeDepart = villeDepart;
        distance = "" + DistanceVilles.getInstance().calculDistance(villeDepart, villeArrivee) + " kms";
    }

    public String getVilleDepart() {
        return villeDepart;
    }
    public String getVilleArrivee() {
        return villeArrivee;
    }
    public String getDistance() {
        return distance;
    }

}


