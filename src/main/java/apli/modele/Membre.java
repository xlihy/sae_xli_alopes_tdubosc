package apli.modele;

import java.util.Iterator;
import java.util.List;

public class Membre {
    private String pseudo;
    private String ville;

    public Membre(String parPseudo, String parVille){
        //Constructeur principal
        pseudo = parPseudo;
        ville = parVille;
    }

    public String getPseudo() {

        return pseudo;
    }

    public String getVille() {

        return ville;
    }

    @Override
    public String toString() {
        return "Membre{" +
                "pseudo='" + pseudo + '\'' +
                ", ville='" + ville + '\'' +
                '}';
    }

}
