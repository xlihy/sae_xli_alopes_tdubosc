package apli.modele;

import java.util.Collections;
import java.util.List;

public class AlgoritmeTousLesSolutionsOrdreCroissant extends  AlgotithmeTouteslesSolutions {
    @Override
    public List<Trajet> calculer(List<VenteDetail> scenario) {
        List<Trajet> trajets = super.calculer(scenario);

        Collections.sort(trajets);
        return  trajets;
    }
}
