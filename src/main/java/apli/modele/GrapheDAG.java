package apli.modele;

import java.util.*;

public class GrapheDAG {

    private HashSet<String> sommetsSupprimes;
    private HashMap<String, Integer> degreEntrantSommets;
    private HashMap<String, List<String>> listeAdjacences;


    public GrapheDAG(List<VenteDetail> scenario) {
        sommetsSupprimes = new HashSet<>();
        degreEntrantSommets = new HashMap<>();
        listeAdjacences = new HashMap<>();

        // Créer graphe
        Iterator<VenteDetail> iterateur = scenario.iterator();
        while (iterateur.hasNext()){
            VenteDetail venteDetail= iterateur.next();
            String villeVendeur = venteDetail.getVilleVendeur();
            String villeAcheteur = venteDetail.getVilleAcheteur();

            List<String> villesAdjacentes = listeAdjacences.get(villeVendeur);

            if (villesAdjacentes == null) {
                villesAdjacentes = new ArrayList<>();
                listeAdjacences.put(villeVendeur,villesAdjacentes);
            }

            villesAdjacentes.add(villeAcheteur);
            // Ajouter les degreEntrants
            ajouterDegreEntrant(villeVendeur, 0);
            ajouterDegreEntrant(villeAcheteur, 1);
        }
    }

    public GrapheDAG(HashSet<String> sommetsSupprimes, HashMap<String, Integer> degreEntrantSommets, HashMap<String, List<String>> listeAdjacences) {
        this.sommetsSupprimes = new HashSet<>(sommetsSupprimes);
        this.degreEntrantSommets = new HashMap<String, Integer>(degreEntrantSommets);
        this.listeAdjacences = listeAdjacences;
    }

    private void ajouterDegreEntrant(String ville, int i) {
        Integer degreEntrant = degreEntrantSommets.get(ville);
        if (degreEntrant == null) {
            degreEntrant = new Integer(i);
            degreEntrantSommets.put(ville, degreEntrant);
        } else {
            degreEntrantSommets.put(ville, new Integer(i + degreEntrant));
        }
    }
    public List<String> trouverToutesLesSources() {
        List<String> sources = new ArrayList<>();
        Iterator<String> iterateur = degreEntrantSommets.keySet().iterator();
        while (iterateur.hasNext()){
            String source = iterateur.next();
            if (!sommetsSupprimes.contains(source)) {
                Integer degreEntrant = degreEntrantSommets.get(source);
                if (degreEntrant == 0){
                    sources.add(source);
                }
            }
        }
        return sources;
    }

    public GrapheDAG creerPlusPetitGrapheSansSource(String source) {
        GrapheDAG petitGraphe = new GrapheDAG(this.sommetsSupprimes, this.degreEntrantSommets, this.listeAdjacences);
        petitGraphe.supprimerVilleSource(source);
        return petitGraphe;
    }

    private void supprimerVilleSource(String source) {
        sommetsSupprimes.add(source);

        List<String> villeAdjacentes = listeAdjacences.get(source);
        if (villeAdjacentes != null) {
            Iterator<String> iterator = villeAdjacentes.iterator();
            while (iterator.hasNext()) {
                String ville = iterator.next();
                ajouterDegreEntrant(ville, -1);
            }
        }
    }
}
