package apli.modele;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmeTest extends Algorithme {
    @Override
    public List<Trajet> calculer(List<VenteDetail> scenario) {
        List<Trajet> listTrajets = new ArrayList<>();

        Trajet trajet1 = new Trajet();
        trajet1.setNom("trajet1");
        trajet1.ajouterVille(scenario.get(0).getVilleAcheteur());
        trajet1.ajouterVille(scenario.get(0).getVilleVendeur());
        listTrajets.add(trajet1);

        Trajet trajet2 = new Trajet();
        trajet2.setNom("trajet2");
        trajet2.ajouterVille(scenario.get(0).getVilleAcheteur());
        trajet2.ajouterVille(scenario.get(0).getVilleVendeur());
        trajet2.ajouterVille(scenario.get(1).getVilleAcheteur());
        trajet2.ajouterVille(scenario.get(1).getVilleVendeur());
        listTrajets.add(trajet2);

        return listTrajets;
    }
}
