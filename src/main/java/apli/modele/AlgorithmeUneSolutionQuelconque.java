package apli.modele;

import java.util.*;

public class AlgorithmeUneSolutionQuelconque extends Algorithme {

    private List<String> ordreTopologique;
    private HashSet<String> sommetsSupprimes;
    private HashMap<String, Integer> degreEntrantSommets;
    private HashMap<String, List<String>> listeAdjacences;


    private void calculerListeOrdreTopologique(List<VenteDetail> scenario) {
        // Créer graphe
        Iterator<VenteDetail> iterateur = scenario.iterator();
        while (iterateur.hasNext()){
            VenteDetail venteDetail= iterateur.next();
            String villeVendeur = venteDetail.getVilleVendeur();
            String villeAcheteur = venteDetail.getVilleAcheteur();

            List<String> villesAdjacentes = listeAdjacences.get(villeVendeur);

            if (villesAdjacentes == null) {
                villesAdjacentes = new ArrayList<>();
                listeAdjacences.put(villeVendeur,villesAdjacentes);
            }

            villesAdjacentes.add(villeAcheteur);
            // Ajouter les degreEntrants
            ajouterDegreEntrant(villeVendeur, 0);
            ajouterDegreEntrant(villeAcheteur, 1);
        }

        // Calcul ordre topologique
        while (ilResteDesVilles()) {
            String villeSource = getUneVilleSource();
            ordreTopologique.add(villeSource);
            supprimerVilleSource(villeSource);
        }
    }

    private boolean ilResteDesVilles() {
        return degreEntrantSommets.size() != sommetsSupprimes.size();
    }
    private String getUneVilleSource() {
        Iterator<String> iterateur = degreEntrantSommets.keySet().iterator();
        while (iterateur.hasNext()){
            String ville = iterateur.next();
            if (!sommetsSupprimes.contains(ville)) {
                Integer degreEntrant = degreEntrantSommets.get(ville);
                if (degreEntrant == 0){
                    return ville;
                }
            }
        }
        return null;
    }
    private void supprimerVilleSource(String villeSource) {
        sommetsSupprimes.add(villeSource);

        List<String> villeAdjacentes = listeAdjacences.get(villeSource);
        if (villeAdjacentes != null) {
            Iterator<String> iterator = villeAdjacentes.iterator();
            while (iterator.hasNext()) {
                String ville = iterator.next();
                ajouterDegreEntrant(ville, -1);
            }
        }

    }



    private void ajouterDegreEntrant(String ville, int i) {
        Integer degreEntrant = degreEntrantSommets.get(ville);
        if (degreEntrant == null) {
            degreEntrant = new Integer(i);
            degreEntrantSommets.put(ville, degreEntrant);
        } else {
            degreEntrantSommets.put(ville, new Integer(i + degreEntrant));
        }
    }

    public AlgorithmeUneSolutionQuelconque() {
        ordreTopologique = new ArrayList<>();
        sommetsSupprimes = new HashSet<>();
        degreEntrantSommets = new HashMap<>();
        listeAdjacences = new HashMap<>();

    }

    @Override
    public List<Trajet> calculer(List<VenteDetail> scenario) {
        calculerListeOrdreTopologique(scenario);

        Trajet trajet = retournerTrajetAPartirOrdreTopologique(ordreTopologique);
        trajet.setNom("Trajet 1");

        List<Trajet> trajets = new ArrayList<>();
        trajets.add(trajet);
        return trajets;
    }

}
