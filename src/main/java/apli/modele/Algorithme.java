package apli.modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Algorithme {
    abstract public List<Trajet> calculer(List<VenteDetail> scenario);

    public Trajet retournerTrajetAPartirOrdreTopologique(List<String> ordreTopologique) {
        Trajet trajet = new Trajet();
        trajet.ajouterVille("Velizy");
        Iterator<String> iterator = ordreTopologique.iterator();
        while (iterator.hasNext()) {
            trajet.ajouterVille(iterator.next());
        }
        trajet.ajouterVille("Velizy");

        List<Trajet> trajets = new ArrayList<>();
        trajets.add(trajet);
        return  trajet;
    }
}
