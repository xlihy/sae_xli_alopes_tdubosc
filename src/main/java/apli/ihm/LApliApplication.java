package apli.ihm;

import apli.ihm.controleurs.ControleurPrincipal;
import apli.ihm.vues.VuePrincipale;
import apli.modele.ListMembres;
import apli.modele.Membre;
import apli.utils.LectureFichiers;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static apli.utils.LectureFichiers.lireFichierMembres;

public class LApliApplication extends Application {

    @Override
    public void start(Stage stage) throws IOException {
        // On récupère les distances entre les villes
        File fichierVilles = LectureFichiers.getFile("distances.txt");
        LectureFichiers.initialiserDistanceVilles(fichierVilles);

        // On récupère les membres et on les met dans ListMembres
        File fichierMembres = LectureFichiers.getFile("membres_APLI.txt");
        List<Membre> listMembres = LectureFichiers.lireFichierMembres(fichierMembres);
        ListMembres.getInstance().addAll(listMembres);



        VuePrincipale vuePrincipale = new VuePrincipale();
        ControleurPrincipal controleurPrincipal = new ControleurPrincipal(vuePrincipale, stage);
        stage.setTitle("L'Apli by BUT Vélizy");
        stage.setScene(new Scene(vuePrincipale.getVue()));

        controleurPrincipal.setVueAccueil();

        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}