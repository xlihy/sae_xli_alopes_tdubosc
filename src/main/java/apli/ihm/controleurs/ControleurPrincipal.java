package apli.ihm.controleurs;

import apli.ihm.vues.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class ControleurPrincipal {

    private VueAccueil vueAccueil = new VueAccueil();
    private VueMembre vueMembre;
    private ControleurMembre controleurMembre;
    private Stage stagePrincipal;
    private VuePrincipale vuePrincipale;
    private VueScenario vueScenario;
    private ControleurScenario controleurScenario;
    private VueCreerScenario vueCreerScenario;
    private ControleurCreerScenario controleurCreerScenario;
    private VueCalculTrajet vueCalculTrajet;
    private ControleurCalculTrajet controleurCalculTrajet;


    //FinancialAccount account;

    public ControleurPrincipal(VuePrincipale vue, Stage stagePrincipal){
        // Constructeur principal
        this.stagePrincipal = stagePrincipal;

        vueMembre = new VueMembre();
        controleurMembre = new ControleurMembre(vueMembre, stagePrincipal);

        vueScenario = new VueScenario();
        controleurScenario = new ControleurScenario(vueScenario, stagePrincipal);

        vueCreerScenario = new VueCreerScenario();
        controleurCreerScenario = new ControleurCreerScenario(vueCreerScenario, stagePrincipal);

        vueCalculTrajet = new VueCalculTrajet();
        controleurCalculTrajet = new ControleurCalculTrajet(vueCalculTrajet, stagePrincipal);

        setVue(vue);
    }

    /**
     * Attache le controler avec la vue passée en argument
     * @param vue
     */
    private void setVue(VuePrincipale vue) {
        vuePrincipale = vue;

        // Gestion de l'entrée membre dans le menu
        vue.getMenuBar().getMenus().get(0).getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                setVueMembre();
            }
        });

        // Gestion de l'entrée scenario dans le menu
        vue.getMenuBar().getMenus().get(1).getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                setVueScenario();
            }
        });

        // Gestion de l'entrée Creer scenario dans le menu
        vue.getMenuBar().getMenus().get(1).getItems().get(1).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                setVueCreerScenario();
            }
        });


        // Gestion de l'entrée CalculTrajet dans le menu
        vue.getMenuBar().getMenus().get(2).getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                setVueCalculTrajet();
            }
        });




        //Gestion de l'entrée Accueil dans le menu
        vue.getMenuBar().getMenus().get(4).getItems().get(0).setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Accueil");
                setVueAccueil();
            }
        });

    }

    public void setVueAccueil() {
            vuePrincipale.setVueApli(vueAccueil.getVue());
        }

    public void setVueMembre() {
        vuePrincipale.setVueApli(vueMembre.getVue());
    }

    public void setVueScenario() {
        vuePrincipale.setVueApli(vueScenario.getVue());
    }
    public void setVueCreerScenario() {
        vuePrincipale.setVueApli(vueCreerScenario.getVue());
    }


    public void setVueCalculTrajet() {
        vuePrincipale.setVueApli(vueCalculTrajet.getVue());
    }

}
