package apli.ihm.controleurs;

import apli.ihm.modeles.ModeleVueCalculTrajet;
import apli.modele.VenteDetail;
import apli.ihm.vues.VueCalculTrajet;
import apli.modele.*;
import apli.utils.IHMUtils;
import apli.utils.LectureFichiers;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ControleurCalculTrajet {

    private static final String[] nomsFichiersScenario = {"scenario_0.txt", "scenario_1_1.txt", "scenario_1_2.txt", "scenario_2_1.txt", "scenario_2_2.txt"};
    private VueCalculTrajet vueCalculTrajet;
    private ModeleVueCalculTrajet modeleVueCalculTrajet;
    private Stage stagePrincipal;

    public ControleurCalculTrajet(VueCalculTrajet vue, Stage stagePrincipal){
        //Constructeur principal
        modeleVueCalculTrajet = new ModeleVueCalculTrajet();
        this.stagePrincipal = stagePrincipal;
        setVue(vue);
    }

    private void setVue(VueCalculTrajet vue) {
        vueCalculTrajet = vue;

        // Lie les données du modèle avec les objets graphiques de la vue
        vue.getCheminFichier().textProperty().bind(modeleVueCalculTrajet.cheminFichierProperty());
        vue.getTableauListTrajetsView().itemsProperty().bind(modeleVueCalculTrajet.listTrajetsProperty());
        vue.getTableauDetailTrajetView().itemsProperty().bind(modeleVueCalculTrajet.listEtapesProperty());
        vue.getDistanceTrajet().textProperty().bind(modeleVueCalculTrajet.distanceTrajetSelectionneProperty());

        // Gestion du bouton ouvrir
        vueCalculTrajet.getBoutonOuvrir().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Bouton ouvrir click");

                FileChooser fileChooser = new FileChooser();
                File fichierChoisi = fileChooser.showOpenDialog(stagePrincipal);

                choixFichierScenario(fichierChoisi);
            }
        });

        // Gestion du combo
        vueCalculTrajet.getComboScenario().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                // trouver l'item selectionné
                int indexCombo = vueCalculTrajet.getComboScenario().getSelectionModel().getSelectedIndex();
                System.out.println("Index: [" + indexCombo + "] ");

                // Récupérer le fichier correspondant
                try {
                    File fichierScenario = LectureFichiers.getFile(nomsFichiersScenario[indexCombo]);
                    choixFichierScenario(fichierScenario);

                } catch (IOException e) {
                }
            }
        });

        // Gestion du bouton valider
        vueCalculTrajet.getBoutonValider().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Bouton valider click");

                // Récupérer le scenario choisi
                List<VenteDetail> listVentes = modeleVueCalculTrajet.getListVentes();
                if (listVentes == null){
                    IHMUtils.afficherMessageInfo("Vous devez sélectionner un scénario");
                    return;
                }

                // Récupérer l'algorithme
                Algorithme algo = getAlgorithme();

                //Récupérer le résultat
                List<Trajet> resultat = algo.calculer(listVentes);
                modeleVueCalculTrajet.setListTrajets(resultat);
            }
        });

        // Choix de la sélection d'un trajet
        ObservableList<Trajet> selectedItems = vueCalculTrajet.getTableauListTrajetsView().getSelectionModel().getSelectedItems();
        selectedItems.addListener(
                new ListChangeListener<Trajet>() {
                    @Override
                    public void onChanged(ListChangeListener.Change<? extends Trajet> change) {
                        System.out.println("Selection changed: " + change.getList());
                        if (change.getList() != null && change.getList().size() > 0) {
                            modeleVueCalculTrajet.setTrajetSelectionne(change.getList().get(0));
                        }
                    }
                });
    }

    private Algorithme getAlgorithme() {
        RadioButton boutonSelectionne = (RadioButton)vueCalculTrajet.getToggleGroupAlgo().getSelectedToggle();
        String algo = boutonSelectionne.getText();
        switch (algo) {
            case "Solution quelconque":
                return new AlgorithmeUneSolutionQuelconque();
            case "Toutes les solutions":
                return new AlgotithmeTouteslesSolutions();
            case "Toutes les solutions (ordre croissant de longueur)":
                return new AlgoritmeTousLesSolutionsOrdreCroissant();
            case "Solution minimisant la longueur parcourue":
                return new AlgorithmeSolutionIdeale();
        }
        // pour le test
        return new AlgorithmeTest();
    }

    private void choixFichierScenario(File fichierChoisi) {
        try {
            List<Vente> listVentes = LectureFichiers.lireFichierScenario(fichierChoisi);
            List<VenteDetail> venteDetailList = VenteDetail.toListVenteDetail(listVentes);
            modeleVueCalculTrajet.setListVentes(venteDetailList);
            modeleVueCalculTrajet.cheminFichierProperty().set(fichierChoisi.getAbsolutePath());

        } catch (IOException e) {
        }
    }

}
