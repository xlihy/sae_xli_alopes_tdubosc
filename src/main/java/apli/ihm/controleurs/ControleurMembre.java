package apli.ihm.controleurs;

import apli.ihm.modeles.ModeleVueMembre;
import apli.ihm.vues.VueAccueil;
import apli.ihm.vues.VueMembre;
import apli.ihm.vues.VuePrincipale;
import apli.modele.ListMembres;
import apli.modele.Membre;
import apli.utils.IHMUtils;
import apli.utils.LectureFichiers;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ControleurMembre {

    private VueMembre vueMembre;
    private ModeleVueMembre modeleVueMembre;
    private Stage stagePrincipal;

    public ControleurMembre(VueMembre vue, Stage stagePrincipal){
        //Constructeur principal
        modeleVueMembre = new ModeleVueMembre();
        modeleVueMembre.setListMembres(ListMembres.getInstance());

        this.stagePrincipal = stagePrincipal;
        setVue(vue);
    }

    private void setVue(VueMembre vue) {
        vueMembre = vue;
        vue.getTableView().itemsProperty().bind(modeleVueMembre.listMembresProperty());
    }

    public ModeleVueMembre getModeleVueMembre() {
        return modeleVueMembre;
    }

    public void setModeleVueMembre(ModeleVueMembre modeleVueMembre) {
        this.modeleVueMembre = modeleVueMembre;
    }



}
