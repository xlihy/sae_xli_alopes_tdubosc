package apli.ihm.controleurs;

import apli.ihm.modeles.ModeleVueScenario;
import apli.modele.VenteDetail;
import apli.ihm.vues.VueScenario;
import apli.modele.Vente;
import apli.utils.IHMUtils;
import apli.utils.LectureFichiers;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ControleurScenario {

    private static final String[] nomsFichiersScenario = {"scenario_0.txt", "scenario_1_1.txt", "scenario_1_2.txt", "scenario_2_1.txt", "scenario_2_2.txt"};
    private VueScenario vueScenario;
    private ModeleVueScenario modeleVueScenario;
    private Stage stagePrincipal;

    public ControleurScenario(VueScenario vue, Stage stagePrincipal){
        //Constructeur principal
        modeleVueScenario = new ModeleVueScenario();
        this.stagePrincipal = stagePrincipal;
        setVue(vue);
    }

    private void setVue(VueScenario vue) {
        // Associe le controleur avec la vue passée en argument.
        // Toutes les associations graphiques sont codées dans cette méthode (lien controleur - vue - modèle)
        vueScenario = vue;

        // Lie les données du modèle avec les objets graphiques de la vue
        vue.getCheminFichier().textProperty().bind(modeleVueScenario.cheminFichierProperty());
        vue.getTableView().itemsProperty().bind(modeleVueScenario.listVentesProperty());

        // Gestion du bouton ouvrir
        vueScenario.getBoutonOuvrir().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Bouton ouvrir click");

                FileChooser fileChooser = new FileChooser();
                File fichierChoisi = fileChooser.showOpenDialog(stagePrincipal);
                afficherFichierSecnario(fichierChoisi);
            }
        });

        // Gestion du combo
        vueScenario.getComboScenario().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                // trouver l'item selectionné
                int indexCombo = vueScenario.getComboScenario().getSelectionModel().getSelectedIndex();
                System.out.println("Index: [" + indexCombo + "] ");

                // Récupérer le fichier correspondant
                try {
                    File fichierScenario = LectureFichiers.getFile(nomsFichiersScenario[indexCombo]);

                    // Afficher
                    afficherFichierSecnario(fichierScenario);


                } catch (IOException e) {
                }
            }
        });
    }

    private void afficherFichierSecnario(File fichierChoisi) {
        if (fichierChoisi != null) {
            try  {
                List<Vente> listVentes = LectureFichiers.lireFichierScenario(fichierChoisi);
                modeleVueScenario.cheminFichierProperty().set(fichierChoisi.getAbsolutePath());

                List<VenteDetail> venteDetailList = VenteDetail.toListVenteDetail(listVentes);
                modeleVueScenario.setScenario(venteDetailList);
            } catch(Exception e) {
                IHMUtils.afficherMessageInfo("Le fichier semble incorrect. Choisissez un nouveau fichier");
            }
        }
    }

}
