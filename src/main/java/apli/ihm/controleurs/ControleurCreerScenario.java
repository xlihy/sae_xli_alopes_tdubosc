package apli.ihm.controleurs;

import apli.ihm.modeles.ModeleVueCreerScenario;
import apli.ihm.modeles.ModeleVueMembre;
import apli.ihm.vues.VueCreerScenario;
import apli.utils.IHMUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.PrintWriter;

import static java.lang.Integer.parseInt;

public class ControleurCreerScenario {

    private VueCreerScenario vueCreerScenario;
    private ModeleVueCreerScenario modeleVueCreerScenario;
    private Stage stagePrincipal;

    public ControleurCreerScenario(VueCreerScenario vue, Stage stagePrincipal){
        //Constructeur principal
        modeleVueCreerScenario = new ModeleVueCreerScenario();
        //modeleCreerScenario.setListMembres(ListMembres.getInstance());

        this.stagePrincipal = stagePrincipal;
        setVue(vue);
    }

    private void setVue(VueCreerScenario vue) {
        vueCreerScenario = vue;

        // Lie les données du modèle avec les objets graphiques de la vue
        vueCreerScenario.getTextField().textProperty().bindBidirectional(modeleVueCreerScenario.nombreAcheteursProperty());

        //Gestion du bouton Valider
        vueCreerScenario.getBoutonValider().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Bouton valider click");
                // Récupère le texte
                try {
                    String nbVentes = modeleVueCreerScenario.getNombreAcheteurs();
                    int nb = Integer.parseInt(nbVentes);
                    // Ajouter les comboBoxs
                    vueCreerScenario.setComboVentes(nb);
                } catch (NumberFormatException e) {
                    IHMUtils.afficherMessageInfo("Vous devez rentrer un nombre entier");
                }
            }
        });

        //Gestion du bouton enregistrer
        vueCreerScenario.getBoutonEnregistrer().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Bouton enregistrer click");
                // Ouvrir un FileChooser et choisir un fichier
                FileChooser fileChooser = new FileChooser();
                File fichierChoisi = fileChooser.showSaveDialog(stagePrincipal);

                if (fichierChoisi != null) {
                    try {
                        PrintWriter writer = new PrintWriter(fichierChoisi);
                        // Pour toutes les comboBoxs,
                        ComboBox[][] tableauxCombo = vueCreerScenario.getTableauxCombo();
                        for (int i = 0; i < tableauxCombo.length; i++) {
                            // récupérer la valeur vendeur et acheteur
                            ComboBox vendeurCombo = tableauxCombo[i][0];
                            ComboBox acheteurCombo = tableauxCombo[i][1];
                            String vendeur = (String)vendeurCombo.getValue();
                            String acheteur = (String)vendeurCombo.getValue();
                            if (vendeur== null | acheteur == null) {
                                IHMUtils.afficherMessageInfo("Vous devez choisir tous les vendeurs et acheteurs");
                                return;
                            }
                            // Ajouter la ligne dans le fichier (xxxx -> yyyyy)
                            writer.println(vendeur + " -> " + acheteur);
                        }

                        writer.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                        IHMUtils.afficherMessageInfo("Erreur lors de l'écriture du fichier");
                    }

                }

            }
        });

        //Gestion des combosBox
    }

}
