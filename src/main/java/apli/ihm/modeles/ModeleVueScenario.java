package apli.ihm.modeles;

import apli.modele.VenteDetail;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

public class ModeleVueScenario {
    private final StringProperty cheminFichier;
    private final ListProperty<VenteDetail> listVentes;

    public ModeleVueScenario() {
        //Constructeur principal
        cheminFichier = new SimpleStringProperty("cliquer sur le bouton ouvrir...");
        listVentes = new SimpleListProperty<VenteDetail>(FXCollections.observableArrayList());
    }

    public ObservableList<VenteDetail> getListVentes() {
        return listVentes.get();
    }

    public ListProperty<VenteDetail> listVentesProperty() {
        return listVentes;
    }

    public StringProperty cheminFichierProperty() {
        return cheminFichier;
    }

    public void setScenario(List<VenteDetail> scenario) {
        listVentes.clear();
        listVentes.addAll(scenario);
    }
}
