package apli.ihm.modeles;

import apli.modele.Etape;
import apli.modele.Trajet;
import apli.modele.VenteDetail;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;

public class ModeleVueCalculTrajet {

    private final StringProperty cheminFichier;
    private final ListProperty<Trajet> listTrajets;
    private Trajet trajetSelectionne;
    private ListProperty<Etape> listEtapes;
    private StringProperty distanceTrajetSelectionne;
    private  List<VenteDetail> listVentes;


    public ModeleVueCalculTrajet() {
        //Constructeur principal
        cheminFichier = new SimpleStringProperty("cliquer sur le bouton ouvrir...");
        listTrajets = new SimpleListProperty<Trajet>(FXCollections.observableArrayList());
        listEtapes = new SimpleListProperty<Etape>(FXCollections.observableArrayList());
        distanceTrajetSelectionne = new SimpleStringProperty("");
    }

    public Trajet getTrajetSelectionne() {
        return trajetSelectionne;
    }

    public ObservableList<Trajet> getListTrajets() {
        return listTrajets.get();
    }

    public ListProperty<Trajet> listTrajetsProperty() {
        return listTrajets;
    }

    public String getCheminFichier() {
        return cheminFichier.get();
    }

    public StringProperty cheminFichierProperty() {
        return cheminFichier;
    }

    public String getDistanceTrajetSelectionne() {
        return distanceTrajetSelectionne.get();
    }

    public StringProperty distanceTrajetSelectionneProperty() {
        return distanceTrajetSelectionne;
    }

    public List<VenteDetail> getListVentes() {
        return listVentes;
    }

    public void setListVentes(List<VenteDetail> listVentes) {
        this.listVentes = listVentes;
    }
    public ObservableList<Etape> getListEtapes() {
        return listEtapes.get();
    }

    public ListProperty<Etape> listEtapesProperty() {
        return listEtapes;
    }

    public void setListTrajets(List<Trajet> parListTrajets) {
        listTrajets.clear();
        listTrajets.addAll(parListTrajets);
    }


    public void setTrajetSelectionne(Trajet trajetSelectionne) {
        this.trajetSelectionne = trajetSelectionne;
        listEtapes.clear();
        //transforme liste de villes en liste d'étapes
        listEtapes.addAll(trajetSelectionne.getListEtapes());
        // Calculer la distance totale
        distanceTrajetSelectionne.set(trajetSelectionne.getDistanceTrajet() + " kms");
    }

}