package apli.ihm.modeles;

import apli.modele.Membre;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

import java.util.List;
import java.util.Map;

public class ModeleVueCreerScenario {

    private StringProperty nombreAcheteurs;

    public ModeleVueCreerScenario() {
        //Constructeur principal
        nombreAcheteurs = new SimpleStringProperty("");
    }

    public String getNombreAcheteurs() {
        return nombreAcheteurs.get();
    }

    public StringProperty nombreAcheteursProperty() {
        return nombreAcheteurs;
    }
}
