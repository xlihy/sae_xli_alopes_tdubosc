package apli.ihm.modeles;

import apli.modele.Membre;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class ModeleVueMembre {

    private final ListProperty<Membre> listMembres;

    public ModeleVueMembre() {
        //Constructeur principal
        listMembres = new SimpleListProperty<Membre>(FXCollections.observableArrayList());
    }
    public ObservableList<Membre> getListMembres() {
        return listMembres.get();
    }

    public ListProperty<Membre> listMembresProperty() {
        return listMembres;
    }

    public void setListMembres(List<Membre> membres) {
        listMembres.clear();
        listMembres.addAll(membres);
    }
}