package apli.ihm.vues;

import apli.modele.Membre;
import apli.modele.Vente;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VueScenario {

    private VBox vue;
    private Button boutonOuvrir;
    private TableView tableView;
    private TextField cheminFichier;
    private ComboBox comboScenario;

    public VueScenario() {
        //Controleur principal
        vue = createVue();
    }

    private VBox createVue(){
        //Permet de creer la vue scenario
        VBox vBox = new VBox(15);
        vBox.setPadding(new Insets(15));

        Label texte = new Label("Sélectionner un fichier scénario :");
        vBox.getChildren().add(texte);

        HBox hBoxScenario = new HBox(15);

        //Choix du fichier scénario avec la comboBox
        VBox vBoxCombo = new VBox(15);
        vBoxCombo.getChildren().add(new Label( "Scénario prédéfini"));
        comboScenario = new ComboBox();
        ObservableList<String> list = FXCollections.observableArrayList("Scénario 0", "Scénario 1.1", "Scénario 1.2", "Scénario 2.1","Scénario 2.2");
        comboScenario.setItems(list);
        vBoxCombo.getChildren().add(comboScenario);
        hBoxScenario.getChildren().add(vBoxCombo);

        hBoxScenario.getChildren().add(new Label("ou"));

        //Choix du fichier scénario
        VBox vBoxFichier = new VBox(15);
        vBoxFichier.getChildren().add(new Label("Ouvrir un fichier scénario"));

        HBox hBoxOuverturFichier = new HBox();
        cheminFichier = new TextField();
        hBoxOuverturFichier.getChildren().add(cheminFichier);
        boutonOuvrir = new Button("Ouvrir");
        hBoxOuverturFichier.getChildren().add(boutonOuvrir);
        vBoxFichier.getChildren().add(hBoxOuverturFichier);
        hBoxScenario.getChildren().add(vBoxFichier);

        vBox.getChildren().add(hBoxScenario);

        //Tableau détaillé d'un scénario
        tableView = new TableView();
        TableColumn<Vente, String> column2 = new TableColumn<>("Acheteur");
        column2.setCellValueFactory(new PropertyValueFactory<>("pseudoAcheteur"));
        TableColumn<Vente, String> column3 = new TableColumn<>("Vendeur");
        column3.setCellValueFactory(new PropertyValueFactory<>("pseudoVendeur"));
        TableColumn<Vente, String> column4 = new TableColumn<>("Ville Acheteur ");
        column4.setCellValueFactory(new PropertyValueFactory<>("villeAcheteur"));
        TableColumn<Vente, String> column5 = new TableColumn<>("Ville Vendeur");
        column5.setCellValueFactory(new PropertyValueFactory<>("villeVendeur"));
        TableColumn<Vente, String> column6 = new TableColumn<>("km");
        column6.setCellValueFactory(new PropertyValueFactory<>("distance"));
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);
        tableView.getColumns().add(column4);
        tableView.getColumns().add(column5);
        tableView.getColumns().add(column6);

        vBox.getChildren().add(tableView);

        return vBox;
    }

    public TableView getTableView() {
        return tableView;
    }

    public TextField getCheminFichier() {
        return cheminFichier;
    }
    public Button getBoutonOuvrir() {
        return boutonOuvrir;
    }
    public ComboBox getComboScenario() {
        return comboScenario;
    }


    public Parent getVue() {
        return vue;
    }

}
