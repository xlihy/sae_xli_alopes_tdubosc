package apli.ihm.vues;

import apli.modele.Etape;
import apli.modele.Trajet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class VueCalculTrajet {

    private Button boutonCreerScenario;
    private Button boutonValider;
    private TableView tableauDetailTrajetView;
    private TableView tableauListTrajetsView;
    private TextField distanceTrajet;
    private ToggleGroup radioGroupSolutions;
    final String [] SOLUTIONS = {"Solution quelconque", "Toutes les solutions", "Toutes les solutions (ordre croissant)", "Chemin le plus court"};
    private VBox vue;
    private ComboBox comboScenario;
    private Button boutonOuvrir;
    private TextField cheminFichier;
    private ToggleGroup toggleGroupAlgo;

    public VueCalculTrajet() {
        //Controleur principal
        vue = createVue();
    }

    private VBox createVue(){
        //Permet de creer la vue CalculTrajet

        VBox vBox = new VBox(15);
        vBox.setPadding(new Insets(15));

//Partie avec le choix des scénarios et le choix de l'algorithme
        Label texte = new Label("Sélectionner un fichier scénario :");
        vBox.getChildren().add(texte);

        HBox hBoxScenario = new HBox(15);

        //Choix du fichier scénario avec la comboBox
        VBox vBoxCombo = new VBox(15);
        vBoxCombo.getChildren().add(new Label( "Scénario prédéfini"));
        comboScenario = new ComboBox();
        ObservableList<String> list = FXCollections.observableArrayList("Scénario 0", "Scénario 1.1", "Scénario 1.2", "Scénario 2.1","Scénario 2.2");
        comboScenario.setItems(list);
        vBoxCombo.getChildren().add(comboScenario);
        hBoxScenario.getChildren().add(vBoxCombo);

        hBoxScenario.getChildren().add(new Label("ou"));

        //Choix du fichier scénario
        VBox vBoxFichier = new VBox(15);
        vBoxFichier.getChildren().add(new Label("Ouvrir un fichier scénario"));

        HBox hBoxOuverturFichier = new HBox();
        cheminFichier = new TextField();
        hBoxOuverturFichier.getChildren().add(cheminFichier);
        boutonOuvrir = new Button("Ouvrir");
        hBoxOuverturFichier.getChildren().add(boutonOuvrir);
        vBoxFichier.getChildren().add(hBoxOuverturFichier);
        hBoxScenario.getChildren().add(vBoxFichier);

        vBox.getChildren().add(hBoxScenario);

        //Faire le choix de l'algorithme
        Label label = new Label("Choisissez la solution souhaitée : ");

        // Groupe
        toggleGroupAlgo = new ToggleGroup();

        // Radio 1:
        RadioButton button1 = new RadioButton("Solution quelconque");
        button1.setToggleGroup(toggleGroupAlgo);
        button1.setSelected(true);

        // Radio 2: .
        RadioButton button2 = new RadioButton("Toutes les solutions");
        button2.setToggleGroup(toggleGroupAlgo);

        // Radio 3: .
        RadioButton button3 = new RadioButton("Toutes les solutions (ordre croissant de longueur)");
        button3.setToggleGroup(toggleGroupAlgo);

        // Radio 4: .
        RadioButton button4 = new RadioButton("Solution minimisant la longueur parcourue");
        button4.setToggleGroup(toggleGroupAlgo);

        vBox.getChildren().addAll(label, button1, button2, button3, button4);

        //Bouton Valider
        boutonValider = new Button("Valider");
        vBox.getChildren().add(boutonValider);

        //Separateur entre les deux parties
        Separator separator = new Separator(Orientation.HORIZONTAL);
        vBox.getChildren().add(separator);

//Partie avec la liste des trajets et le détail d'un trajet sélectionné
        HBox hBox2 = new HBox(50);
//#todo utiliser Accordion
        //Liste des Trajets
        VBox vBoxListeTrajet = new VBox(15);

        vBoxListeTrajet.getChildren().add(new Label( "Listes des Trajets:"));

        tableauListTrajetsView = new TableView();
        TableColumn<Trajet, String> columnNom = new TableColumn<>("Trajets");
        columnNom.setCellValueFactory(
                new PropertyValueFactory<>("nom"));
        tableauListTrajetsView.getColumns().add(columnNom);


        vBoxListeTrajet.getChildren().add(tableauListTrajetsView);

        hBox2.getChildren().add(vBoxListeTrajet);

        //Detail d'un trajet sélectionné et le nombre de km total
        VBox vBoxDetailsTrajet = new VBox(15);
        vBoxDetailsTrajet.getChildren().add(new Label( "Détails trajet sélectionné:"));
        HBox.setHgrow(vBoxDetailsTrajet, Priority.ALWAYS);



        tableauDetailTrajetView = new TableView();
        TableColumn<Etape, String> column1 = new TableColumn<>("Ville Départ");
        column1.setCellValueFactory(new PropertyValueFactory<>("villeDepart"));
        TableColumn<Etape, String> column2 = new TableColumn<>("Ville Arrivée");
        column2.setCellValueFactory(new PropertyValueFactory<>("villeArrivee"));
        TableColumn<Etape, String> column3 = new TableColumn<>("KM ");
        column3.setCellValueFactory(new PropertyValueFactory<>("distance"));
        tableauDetailTrajetView.getColumns().add(column1);
        tableauDetailTrajetView.getColumns().add(column2);
        tableauDetailTrajetView.getColumns().add(column3);

        vBoxDetailsTrajet.getChildren().add(tableauDetailTrajetView);
        vBoxDetailsTrajet.getChildren().add(new Label( "Total km trajet:"));

        distanceTrajet = new TextField();
        vBoxDetailsTrajet.getChildren().add(distanceTrajet);


        hBox2.getChildren().add(vBoxDetailsTrajet);

        vBox.getChildren().add(hBox2);
        return vBox;
    }

    public Button getBoutonCreerScenario() {
        return boutonCreerScenario;
    }


    public Button getBoutonValider() {
        return boutonValider;
    }

    public TextField getDistanceTrajet() {
        return distanceTrajet;
    }

    public ComboBox getComboScenario() {
        return comboScenario;
    }

    public Button getBoutonOuvrir() {
        return boutonOuvrir;
    }

    public TextField getCheminFichier() {
        return cheminFichier;
    }

    public TableView getTableauDetailTrajetView() {
        return tableauDetailTrajetView;
    }

    public TableView getTableauListTrajetsView() {
        return tableauListTrajetsView;
    }
    public ToggleGroup getToggleGroupAlgo() {
        return toggleGroupAlgo;
    }


    public Parent getVue() {
        return vue;
    }

}

