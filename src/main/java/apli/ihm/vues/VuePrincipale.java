package apli.ihm.vues;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.VBox;

public class VuePrincipale {
    //View Nodes
    private MenuBar menuBar;
    private VBox vue;

    public VuePrincipale() {
        //Constructeur principal
        vue = createVue();
    }


    private VBox createVue(){
        //Permet de creer la vue principal (le menu)
        VBox vBox = new VBox(15);
        vBox.setPrefWidth(800);
        vBox.setPrefHeight(750);
        vBox.setPadding(new Insets(15));

        //Mise en place du menu Membres
        menuBar = new MenuBar();
        Menu menuMembre = new Menu("Membres");
        RadioMenuItem membre = new RadioMenuItem("Afficher");
        membre.setAccelerator(KeyCombination.keyCombination("Ctrl+M"));

        menuMembre.getItems().add(membre);
        menuBar.getMenus().add(menuMembre);

        //Mise en place du menu Scénario
        Menu menuScenario = new Menu("Scénario");
        MenuItem afficher = new MenuItem("Afficher");
        afficher.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        MenuItem creer= new MenuItem("Créer");
        creer.setAccelerator(KeyCombination.keyCombination("Ctrl+Q"));

        menuScenario.getItems().add(afficher);
        menuScenario.getItems().add(creer);
        menuBar.getMenus().add(menuScenario);

        //Mise en place du menu Trajet
        Menu menuTrajet = new Menu("Trajets");
        MenuItem calcul = new MenuItem("Calcul");
        calcul.setAccelerator(KeyCombination.keyCombination("Ctrl+T"));

        menuTrajet.getItems().add(calcul);
        menuBar.getMenus().add(menuTrajet);

        //Mise en place d'une menu vide pour la place de Accueil
        Menu vide = new Menu("                                                                                                                                               ");
        menuBar.getMenus().add(vide);

        //Mise en place du menu Accueil
        Menu menuAccueil = new Menu("Accueil");
        MenuItem accueil = new MenuItem("Accueil");

        menuAccueil.getItems().add(accueil);

        menuAccueil.setGraphic(new ImageView("home.png"));
        menuBar.getMenus().add(menuAccueil);
        accueil.setAccelerator(KeyCombination.keyCombination("Ctrl+W"));


        //Mise en place de la menuBar et d'un label vide dans la Vbox
        vBox.getChildren().add(menuBar);
        vBox.getChildren().add(new Label());


        //////////////////////////ContextMenu///////////////////////////////
        /*
        Label label = new Label();
        ContextMenu contextMenu = new ContextMenu();

        MenuItem item1 = new MenuItem("Menu Item 1");
        MenuItem item2 = new MenuItem("Menu Item 2");

        item1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                label.setText("Select Menu Item 1");
            }
        });


        item2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                label.setText("Select Menu Item 2");
            }
        });

        contextMenu.getItems().addAll(item1, item2);
        vBox.getChildren().add(contextMenu);
        */
        ////////////////////////////////////////////////////////////////////////////////////
        return vBox;
    }

    public MenuBar getMenuBar() {

        return menuBar;
    }

    public Parent getVue() {

        return vue;
    }

    public void setVueApli(Parent vueApli) {

        vue.getChildren().set(1, vueApli);
    }
}
