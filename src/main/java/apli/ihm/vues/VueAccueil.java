package apli.ihm.vues;

import apli.utils.LectureFichiers;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.text.Font;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class VueAccueil {

    private VBox vue;

    public VueAccueil() {
        //Constructeur principal
        vue = createVue();
    }


    private VBox createVue(){
        //Permet de creer la vue Accueil
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.BASELINE_CENTER);
        vBox.setPadding(new Insets(15));
        try {
            File fichier = LectureFichiers.getFile("pokemon.jpg");

            FileInputStream input = new FileInputStream(fichier);
            Image image = new Image(input);
            ImageView imageView = new ImageView(image);
            vBox.getChildren().add(imageView);
            Label label1 = new Label("Association des Pokémonistes Libres");
            label1.setFont(new Font("French Script MT", 50));
            label1.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent e) {
                    label1.setRotate(45);
                }
            });
            vBox.getChildren().add(label1);

        } catch (IOException io) {
            io.printStackTrace();;
        }
        return vBox;
    }


    public Parent getVue() {
        return vue;
    }

}
