package apli.ihm.vues;

import apli.modele.Membre;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VueMembre {

    private TableView tableView;
    private VBox vue;

    public VueMembre() {
    //Controleur principal
        vue = createVue();
    }

    private VBox createVue(){
        //Permet de creer la vue membre
        VBox vBox = new VBox(15);
        vBox.setPadding(new Insets(15));

        tableView = new TableView();
        TableColumn<Membre, String> column1 =
                new TableColumn<>("Pseudo");
        column1.setCellValueFactory(
                new PropertyValueFactory<>("pseudo"));
        TableColumn<Membre, String> column2 =
                new TableColumn<>("Ville");
        column2.setCellValueFactory(
                new PropertyValueFactory<>("ville"));
        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);

        vBox.getChildren().add(tableView);

        return vBox;
    }

    public TableView getTableView() {
        return tableView;
    }

    public Parent getVue() {
        return vue;
    }

}
