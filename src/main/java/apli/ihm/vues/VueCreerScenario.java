package apli.ihm.vues;

import apli.modele.ListMembres;
import apli.modele.Membre;
import apli.modele.Vente;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.Iterator;

public class VueCreerScenario {

    private VBox vue;
    private TextField textField;
    private Button boutonValider;
    private Button boutonEnregistrer;

    public ComboBox[][] getTableauxCombo() {
        return tableauxCombo;
    }

    private ComboBox[][] tableauxCombo;
    private VBox vBox;
    public VueCreerScenario() {
        //Controleur principal
        vue = createVue();
    }

    private VBox createVue(){
        //Permet de creer la vue scenario
        vBox = new VBox(15);
        vBox.setPadding(new Insets(15));

        //Choix du nombre d'acheteur et de vendeur
        vBox.getChildren().add(new Label("Nombres acheteurs/vendeurs:"));
        HBox hBox = new HBox(15);
        textField = new TextField();
        textField.setEditable(true);

        hBox.getChildren().add(textField);
        boutonValider = new Button("Valider");
        hBox.getChildren().add(boutonValider);
        vBox.getChildren().add(hBox);

        vBox.getChildren().add(new Label(""));

        //Bouton enregister
        boutonEnregistrer = new Button("Enregistrer");
        vBox.getChildren().add(boutonEnregistrer);

        return vBox;
    }

    public void setComboVentes (int nbAcheteurs) {
        tableauxCombo = new ComboBox[nbAcheteurs][2];
        //Permet de creer les lignes avec les deux combosBox en fonction du nb de ventes
        VBox vBoxCombo = new VBox(15);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setStyle("-fx-padding: 0;");
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setContent(vBoxCombo);
        HBox hBoxLabel = new HBox(110);
        hBoxLabel.getChildren().add(new Label("Vendeur"));
        hBoxLabel.getChildren().add(new Label("Acheteur"));
        vBoxCombo.getChildren().add(hBoxLabel);
        for (int i = 0 ; i  < nbAcheteurs ; i++){
            HBox hBox = new HBox(15);
            ComboBox comboVendeur = creerComboMembres();
            hBox.getChildren().add(comboVendeur);
            ComboBox comboAcheteur = creerComboMembres();
            hBox.getChildren().add(comboAcheteur);
            vBoxCombo.getChildren().add(hBox);

            ComboBox[] tab = new ComboBox[2];
            tab[0] = comboVendeur;
            tab[1] = comboAcheteur;
            tableauxCombo[i] = tab;
        }
        vBox.getChildren().set(2, scrollPane);
    }

    private ComboBox creerComboMembres () {
        ComboBox comboBox = new ComboBox();

        Iterator<Membre> iterateur = ListMembres.getInstance().iterator();
        while (iterateur.hasNext()) {
            Membre membre = iterateur.next();
            comboBox.getItems().add(membre.getPseudo());
        }

        return comboBox;
    }


    public Parent getVue() {
        return vue;
    }
    public TextField getTextField() {
        return textField;
    }

    public Button getBoutonValider() {
        return boutonValider;
    }

    public Button getBoutonEnregistrer() {
        return boutonEnregistrer;
    }

}
