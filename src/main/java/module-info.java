module apli.ihm {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens apli.ihm to javafx.fxml;
    exports apli.ihm;
    exports apli.ihm.modeles;
    exports apli.modele;
}